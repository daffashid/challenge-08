const ApplicationController = require("./ApplicationController");
const { NotFoundError } = require("../errors");


describe("ApplicationController", () => {
    describe("#handleGetRoot", () => {
      it("should call res.status(200) and res.json with list of task instances", async () => {
        const status = "OK"
        const message = "BCR API is up and running!"
  
        const mockRequest = {};
  
        const tasks = [{
            status:status,
            message:message
        }];
  
        const mockResponse = {
          status: jest.fn().mockReturnThis(),
          json: jest.fn().mockReturnThis(),
        };
  
        const applicationController  = new ApplicationController ();
  
        await applicationController.handleGetRoot(mockRequest, mockResponse);
  
        expect(mockResponse.status).toHaveBeenCalledWith(200);
        expect(mockResponse.json).toHaveBeenCalledWith(tasks[0]);
      });
    });

    describe("#handleNotFound", () => {
        it("should call res.status(404) and res.json error", async () => {
            const mockRequest = {
                method: jest.fn().mockReturnThis(),
                url: jest.fn().mockReturnThis(),
            };
          
          const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
          };

          const error  = new NotFoundError(mockRequest.method, mockRequest.url);
          const err = { 
            error : {
            name: error.name,
            message: error.message,
            details: error.details
          }
        }

          const applicationController  = new ApplicationController ();
          await applicationController.handleNotFound(mockRequest, mockResponse);
    
          expect(mockResponse.status).toHaveBeenCalledWith(404);
          expect(mockResponse.json).toHaveBeenCalledWith(err)

        });
      });

      describe("#handleError", () => {
        it("should call res.status(500) and and res.json error", async () => {  
          const err = new Error();

          const error = {
            error: {
                name: err.name,
                message: err.message,
                details: err.details || null,
              }
          }
          const mockRequest = {};
    
          const mockResponse = {
            status: jest.fn().mockReturnThis(),
            json: jest.fn().mockReturnThis(),
          };
    
          const applicationController  = new ApplicationController ();
    
          await applicationController.handleError(err,mockRequest, mockResponse);
    
          expect(mockResponse.status).toHaveBeenCalledWith(500);
          expect(mockResponse.json).toHaveBeenCalledWith(error);
        });
      });

      describe("#getOffsetFromRequest", () => {
        it("should call res.status(404) and and res.json error", async () => {  
          const query = 0
          const mockRequest = {query};
    
          const offset = query
    
          const applicationController  = new ApplicationController ();
          const result =await applicationController.getOffsetFromRequest(mockRequest);
    
          expect(result).toBe(offset);
        });
      });

      describe("#getOffsetFromRequest", () => {
        it("should call res.status(404) and and res.json error", async () => {  
          const count = 0
          const query = { page : 1, pageSize : 10 };
          const mockRequest = {query};

          const pageCount = Math.ceil(count / query.pageSize);
          const offset = {
            page : query.page,
            pageCount,
            pageSize : query.pageSize,
            count,
          }
    
          const applicationController  = new ApplicationController ();
          const result =await applicationController.buildPaginationObject(mockRequest, count);
    
          expect(result).toStrictEqual(offset);
        });
      });
});