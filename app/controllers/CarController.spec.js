const CarController = require("./CarController");
const { Car } = require("../models")

describe("CarController", () => {
    describe("#handleListCars", () => {
      it("should call res.status(200) and res.json", async () => {
        const name = "Duster 360";
            const prompt = "Imprezza";

            const mockRequest = {
                params: {
                    id:39
                },
                query:{
                    page:1,
                    pageSize:0
                }
            }
            const carCount = 10;
            const pageCount = Math.ceil(carCount/mockRequest.query.pageSize)
            const pagination = {
                page: mockRequest.query.page,
                pageCount,
                pageSize: mockRequest.query.pageSize,
                count: carCount,
            }

            const mockCar = new Car({id: mockRequest.params.id, name, prompt});
            const mockCarModel = {findAll: jest.fn().mockReturnValue(mockCar), count: jest.fn().mockReturnValue(10)}
            const mockResponse = {
                status: jest.fn().mockReturnThis(),
                json: jest.fn().mockReturnThis()
            };

            const app = new CarController({carModel: mockCarModel})
            await app.handleListCars(mockRequest, mockResponse);
            const cars = mockCarModel.findAll;
            const mockMobil = jest.fn(() => {
                return cars;
            })
            const mobils = jest.fn();
            mobils.mockReturnValue(mockMobil)
            const query = await app.getListQueryFromRequest(mockRequest)

            const baru = {
                cars: mobils,
                meta: {
                    pagination
                }
            }

            expect(mockResponse.status).toHaveBeenCalledWith(200);
            expect(mockResponse.json).toHaveBeenCalledWith(baru)
      });
    });

});